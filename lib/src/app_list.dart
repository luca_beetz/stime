import 'package:flutter/material.dart';
import 'package:device_apps/device_apps.dart';

class AppList extends StatelessWidget {
  final List<Application> apps;
  final Function callback;
  final int selectedIndex;

  AppList({this.apps, this.callback, this.selectedIndex, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemBuilder: (BuildContext context, int index) {
        return Material(
          color: selectedIndex == index ? Colors.orange : Colors.transparent,
          child: InkWell(
            onTap: () => callback(index, apps[index].packageName),
            child: ListTile(
              title: Text(
                apps[index].appName,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              contentPadding: EdgeInsets.symmetric(horizontal: 16.0),
            ),
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return Container(
          height: 8.0,
          color: Colors.white,
        );
      },
      itemCount: this.apps.length,
    );
  }
}