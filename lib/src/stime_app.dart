import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:device_apps/device_apps.dart';
import 'package:shared_preferences/shared_preferences.dart';

import './app_list.dart';

class StimeApp extends StatefulWidget {
  @override
  _StimeAppState createState() => _StimeAppState();
}

class _StimeAppState extends State<StimeApp> {
  static const SCHEDULED_NOTIFICATION_ID = 42;

  bool _timerStarted;
  FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin;

  List<Application> _apps;
  int _selectedAppIndex;

  String _selectedAppPackageName;

  @override
  void initState() {
    super.initState();

    _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    var initializationSettingsAndroid = AndroidInitializationSettings('ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings();
    var initializationSettings = InitializationSettings(
      initializationSettingsAndroid,
      initializationSettingsIOS
    );

    _flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onSelectNotification: _onSelectNotification,
    );

    _timerStarted = false;

    _selectedAppIndex = -1;

    _checkPendingRequests();

    _getInstalledApps();
  }

  @override
  Widget build(BuildContext context) {
    if (_apps == null) return Container(color: Colors.white);

    return Material(
      child: Container(
        color: Colors.white,
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                flex: 5,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Material(
                    elevation: 8.0,
                    color: Color.fromRGBO(40, 40, 40, 1),
                    child: Container(
                      child: InkWell(
                        child: Icon(
                          _timerStarted ? Icons.pause : Icons.play_arrow,
                          color: Colors.white,
                          size: 96.0,
                        ),
                        onTap: _toggleTimer,
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 5,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Material(
                    elevation: 8.0,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Color.fromRGBO(40, 40, 40, 1),
                          width: 8.0,
                        ),
                      ),
                      child: Container(
                      color: Color.fromRGBO(40, 40, 40, 1),
                        child: AppList(
                          apps: _apps,
                          selectedIndex: _selectedAppIndex,
                          callback: (int index, String packageName) async {
                            print('Clicked: $packageName with index: $index');
                            setState(() {
                              _selectedAppIndex = index;
                            });
                            _selectedAppPackageName = _apps[index].packageName;

                            SharedPreferences prefs = await SharedPreferences.getInstance();
                            prefs.setString('selectedPackageName', _apps[index].packageName);
                          },
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _checkPendingRequests() async {
    var pendingNotificationRequests = await _flutterLocalNotificationsPlugin.pendingNotificationRequests();

    for (var i = 0; i < pendingNotificationRequests.length; i++) {
      if (pendingNotificationRequests[i].id == SCHEDULED_NOTIFICATION_ID) {
        setState(() {
          _timerStarted = true;
        });
      }
    }
  }

  void _toggleTimer() async {
    print("Timer should be toggled");

    setState(() {
      _timerStarted = !_timerStarted;
    });

    if (_timerStarted) {
      debugPrint("Start notification timer");

      var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'repeating channel id',
        'repeating channel name',
        'repeating description',
        importance: Importance.Max,
        priority: Priority.High,
        onlyAlertOnce: false,
      );

      var iOSPlatformChannelSpecifics = IOSNotificationDetails();

      var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics,
        iOSPlatformChannelSpecifics
      );

      await _flutterLocalNotificationsPlugin.periodicallyShow(
        SCHEDULED_NOTIFICATION_ID,
        'Hey!',
        'Click here to open Signal',
        RepeatInterval.EveryMinute,
        platformChannelSpecifics
      );
    } else {
      debugPrint("Clear notifications");
      _flutterLocalNotificationsPlugin.cancel(SCHEDULED_NOTIFICATION_ID);
      _flutterLocalNotificationsPlugin.cancelAll();
    }
  }

  void _getInstalledApps() async {
    List<Application> apps = await DeviceApps.getInstalledApplications(
      onlyAppsWithLaunchIntent: true,
      includeAppIcons: true,
    );

    // Sort apps by their names
    apps.sort((Application a, Application b) => a.appName.compareTo(b.appName));

    SharedPreferences prefs = await SharedPreferences.getInstance();
    _selectedAppPackageName = prefs.getString('selectedPackageName') ?? apps[0].packageName;

    for (var i = 0; i < apps.length; i++) {
      if (apps[i].packageName == _selectedAppPackageName) {
        _selectedAppIndex = i;
      }
    }

    setState(() {
      _apps = apps;
    });
  }

  Future _onSelectNotification(String payload) async {
    debugPrint('Notification payload: $payload');
    DeviceApps.openApp(_selectedAppPackageName);
  }
}